export function getIconByCondition(condition: string): string {
    return new URL(`/src/assets/${condition}.png`, import.meta.url).href
}