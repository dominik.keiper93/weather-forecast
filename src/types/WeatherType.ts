export type Weather = {
    wind_direction_60: string;
    wind_speed_60: number;
    pressure_msl: number;
    relative_humidity: number;
    condition: string;
    temperature: number;
    icon: string;
    timestamp: string;
};
