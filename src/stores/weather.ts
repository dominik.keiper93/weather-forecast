import {defineStore} from "pinia";
import type {Weather} from "@/types/WeatherType";
import type {Location} from "@/types/LocationType";

export const useWeatherStore = defineStore("weather", {
    state: () => ({
        location: {
            lat: 0.0,
            lon: 0.0,
        } as Location,
        forecast: [] as Weather[],
        current: {} as Weather,
    }),
    getters: {
        isValid: (state) =>
            String(state.location.lon).length > 8 &&
            String(state.location.lat).length > 8,
    },
    actions: {
        getWeatherData(loc: string | null): void {
            const api: string = "https://api.brightsky.dev/";
            const currentDate: string = new Date().toJSON().slice(0, 10);
            const forecastFetches: string[] = [];
            let lat, lon;

            if (loc) {
                if (loc === "MU") {
                    lat = 48.137154;
                    lon = 11.576124;
                }
                if (loc === "BE") {
                    lat = 52.520008;
                    lon = 13.404954;
                }
                if (loc === "WI") {
                    lat = 50.078217;
                    lon = 8.239761;
                }
            } else {
                lat = this.location.lat;
                lon = this.location.lon;
            }

            fetch(api + `current_weather?lat=${lat}&lon=${lon}&date=${currentDate}`)
                .then((data) => data.json())
                .then((response) => (this.current = response.weather));

            for (let i: number = 0; i < 5; i++) {
                const newDate = new Date(new Date().setDate(new Date().getDate() + i + 1))
                    .toJSON()
                    .slice(0, 10);
                forecastFetches.push(
                    api + `weather?lat=${lat}&lon=${lon}&date=${newDate}`,
                );
            }

            Promise.all([
                fetch(forecastFetches[0]),
                fetch(forecastFetches[1]),
                fetch(forecastFetches[2]),
                fetch(forecastFetches[3]),
                fetch(forecastFetches[4]),
            ])
                .then(async ([data1, data2, data3, data4, data5]) => {
                    const a = await data1.json();
                    const b = await data2.json();
                    const c = await data3.json();
                    const d = await data4.json();
                    const e = await data5.json();
                    return [a, b, c, d, e];
                })
                .then(
                    (response) =>
                        (this.forecast = response.map((e) =>
                            e.weather.reduce((prev, current) =>
                                +prev.id > +current.id ? prev : current,
                            ),
                        )),
                );
        },
    },
});
